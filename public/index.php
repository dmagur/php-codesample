<?php
    error_reporting(E_ALL);
    require __DIR__ . '/../vendor/autoload.php';
    use Check24Blog\Core\Application;
    use Check24Blog\Core\Config;
    use Check24Blog\Core\MySqliDatabase;
    use Check24Blog\Services\EmailService;

    session_start();
    try {
        $routes = new Config('routes');

        $dbconfig = new Config('database');
        $dbconnection = new MySqliDatabase($dbconfig);

        $smarty = new Smarty();
        $smarty->setTemplateDir('../views/');
        $smarty->setCompileDir('../views/templates_c/');

        $emailconfig = new Config('email');
        $email_service = new EmailService($emailconfig);

        $app = new Application(($_REQUEST['action'])??'default',$routes,$dbconnection,$smarty,$email_service);
        $app->run();
    }
    catch (Exception $e){
        var_dump($e);
    }
    session_write_close();
<?php
    $config = array(
        'default' => array(
            'class' => 'CodesampleBlog\Controllers\HomeController',
            'method' => 'surface'
        ),
        'home' => array(
            'class' => 'CodesampleBlog\Controllers\HomeController',
            'method' => 'index'
        ),
        'login' => array(
            'class' => 'CodesampleBlog\Controllers\LoginController',
            'method' => 'index'
        ),
        'logout' => array(
            'class' => 'CodesampleBlog\Controllers\LogoutController',
            'method' => 'index'
        ),
        'add-entry' => array(
            'class' => 'CodesampleBlog\Controllers\PostController',
            'method' => 'add'
        ),
        'post-details' => array(
            'class' => 'CodesampleBlog\Controllers\PostController',
            'method' => 'details'
        ),
        'author-posts' => array(
            'class' => 'CodesampleBlog\Controllers\AuthorController',
            'method' => 'posts'
        ),
        'imprint' => array(
            'class' => 'CodesampleBlog\Controllers\ImprintController',
            'method' => 'index'
        ),
        'delete-comment' => array(
            'class' => 'CodesampleBlog\Controllers\CommentController',
            'method' => 'delete'
        ),
        'delete-post' => array(
            'class' => 'CodesampleBlog\Controllers\PostController',
            'method' => 'delete'
        ),
    );
    return $config;
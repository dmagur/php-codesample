<?php
namespace CodesampleBlog\Services;

use CodesampleBlog\Contracts\ConfigProvider;
use CodesampleBlog\Contracts\EmailServiceContract;

/**
 * Class EmailService
 * @package CodesampleBlog\Services
 */
class EmailService implements EmailServiceContract {
    private $email_config;

    function __construct(ConfigProvider $email_config)
    {
        $this->email_config = $email_config;
    }

    /**
     * @param string $to
     * @param string $subject
     * @param string $body
     * @return bool
     */
    function send(string $to,string $subject,string $body):bool{
        $from = $this->email_config->get('from_email');
        return mail($to,$subject,$body,"From: $from\r\n");
    }
}
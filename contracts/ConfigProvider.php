<?php
namespace CodesampleBlog\Contracts;

/**
 * Interface ConfigProvider
 * @package CodesampleBlog\Contracts
 */
interface ConfigProvider{
    /**
     * @param string $name
     * @return mixed
     */
    public function get(string $name);
}
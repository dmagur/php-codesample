<?php
namespace CodesampleBlog\Contracts;

/**
 * Interface ApplicationContract
 * @package CodesampleBlog\Contracts
 */
interface ApplicationContract{
    /**
     * @return mixed
     */
    public function run();
}
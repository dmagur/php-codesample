<?php
namespace CodesampleBlog\Contracts;

/**
 * Interface StorageProvider
 * @package CodesampleBlog\Contracts
 */
interface StorageProvider{
    /**
     * @return mixed
     */
    public function connect();
}
<?php
namespace CodesampleBlog\Contracts;

/**
 * Interface ModelContract
 * @package CodesampleBlog\Contracts
 */
interface ModelContract{
    /**
     * @param array $data
     * @return int
     */
    public function insert(array $data): int;

    /**
     * @param array $data
     * @return bool
     */
    public function update(array $data): bool;

    /**
     * @param int $id
     * @return array|null
     */
    public function get(int $id): ?array;

    /**
     * @param string $key
     * @param string $value
     * @return array|null
     */
    public function get_by_key(string $key,string $value):?array;

    /**
     * @param array $params
     * @return array|null
     */
    public function get_list(array $params):?array;
}
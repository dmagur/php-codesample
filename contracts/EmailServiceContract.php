<?php
namespace CodesampleBlog\Contracts;

/**
 * Interface EmailServiceContract
 * @package CodesampleBlog\Contracts
 */
interface EmailServiceContract{
    /**
     * @return mixed
     */
    public function send(string $to,string $subject,string $body);
}
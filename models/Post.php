<?php
namespace CodesampleBlog\Models;

use CodesampleBlog\Core\MySqliModel;
class Post extends MySqliModel{
    /**
     * @var array required field, used for validation
     */
    protected $required = array(
        'title',
        'post_date',
        'content'
    );

    /**
     * @var string database table name
     */
    protected $table = 'post';

    /**
     * @param array $params
     * @return bool|mixed
     */
    public function get_list(array $params):?array {
        $sql = "SELECT `post`.*,CONCAT(`user`.first_name,' ',`user`.last_name) as author FROM `".$this->table."` INNER JOIN `user` ON `user`.id=`post`.user_id";
        $sql = $this->__prepare_sql($sql,$params);
        $res = $this->db->query($sql);
        if ($res) {
            $total_records = $this->get_count($params);
            return ['records' => $res->fetch_all(MYSQLI_ASSOC),'total_records' => $total_records];
        }
        else
            return null;
    }

    /**
     * @param array $params
     * @return int
     */
    public function get_count(array $params):int{
        $sql = "SELECT count(*) as total_records from `".$this->table."` INNER JOIN `user` ON `user`.id=`post`.user_id";

        if (isset($params['limit'])) unset($params['limit']);
        if (isset($params['orderby'])) unset($params['orderby']);

        $sql = $this->__prepare_sql($sql,$params);

        $res = $this->db->query($sql);

        if ($res) {
            $count = $res->fetch_assoc();
            return $count['total_records'];
        }
        else
            return null;
    }

    /**
     * @param $id
     * @return array|null
     */
    public function get_posts_by_author_id($id,$page,$pagesize):?array {
        $id = $this->db->real_escape_string($id);

        $offset = ($page-1)*$pagesize;

        $params = ['where' => "WHERE user_id='$id'",'orderby' => 'order by post_date desc, id desc','limit' => "LIMIT $pagesize OFFSET $offset"];
        return $this->get_list($params);
    }

    /**
     * @param string $sql
     * @param array $params
     * @return string
     */
    private function __prepare_sql(string $sql,array $params):string {
        if (isset($params['where'])){
            $sql .= " ".$params['where'];
        }

        if (isset($params['orderby'])){
            $sql .= " ".$params['orderby'];
        }

        if (isset($params['limit'])){
            $sql .= " ".$params['limit'];
        }
        return $sql;
    }
}
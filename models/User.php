<?php
namespace CodesampleBlog\Models;

use CodesampleBlog\Core\MySqliModel;
use CodesampleBlog\Exceptions\ModelException;

/**
 * Class User
 * @package CodesampleBlog\Models
 */
class User extends MySqliModel{
    /**
     * @var string
     */
    protected $table = 'user';

    /**
     * @param $email
     * @param $pass
     * @return array
     * @throws ModelException if error
     */
    function login(string $email,string $pass):?array {
        if ($stmt = $this->db->prepare("SELECT id,password FROM `user` WHERE `email`=?")) {
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $stmt->bind_result($id,$dbpass);
            $stmt->fetch();
            if ($id){
                if (password_verify($pass, $dbpass)) {
                    $_SESSION['uid'] = $id;
                }
                else
                    return array('error' => 'Password incorrect');
            }
            else{
                return array('error' => 'User with such email not found');
            }
            
            $stmt->close();
            return null;
        }
        else{
            throw new ModelException('Error on login');
        }
    }
}
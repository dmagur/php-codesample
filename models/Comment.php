<?php
namespace CodesampleBlog\Models;

use CodesampleBlog\Core\MySqliModel;

class Comment extends MySqliModel {
    /**
     * @var array required fields, used for validation
     */
    protected $required = array(
        'name',
        'comment'
    );

    /**
     * @var string database table name
     */
    protected $table = 'comment';
}
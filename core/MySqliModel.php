<?php
namespace CodesampleBlog\Core;

use CodesampleBlog\Exceptions\ModelException;
use CodesampleBlog\Contracts\ModelContract;

class MySqliModel implements ModelContract{
    /**
     * @var \mysqli
     */
    protected $db;

    /**
     * MySqliModel constructor.
     * @param \mysqli $db
     */
    function __construct(\mysqli $db)
    {
        $this->db = $db;
    }

    /**
     * method for validating input
     * @param $data
     * @return array
     */
    public function validate(array $data): array {
        $errors = array();
        foreach ($this->required as $r)
            if (!isset($data[$r]) or empty($data[$r])) $errors[$r] = 1;

        return $errors;
    }

    /**
     * @param array $data
     * @return int
     * @throws ModelException if storage error
     */
    public function insert(array $data): int{
        foreach ($data as $key => $value){
            $data[$key] = $this->db->real_escape_string($data[$key]);
        }
        if ($this->db->query("INSERT INTO `".$this->table."`(".implode(",",array_keys($data) ).") VALUES('".implode("','",array_values($data))."')"))
            return $this->db->insert_id;
        else
            throw new ModelException($this->db->error);
    }

    /**
     * @param array $data
     * @return bool
     * @throws ModelException if storage error
     */
    public function update(array $data): bool{
        $id = $data['id'];
        $id = $this->db->real_escape_string($id);
        unset($data['id']);
        $values = [];
        foreach ($data as $key => $value){
            $data[$key] = $this->db->real_escape_string($data[$key]);

            $values[] = $key."='".$value."'";
        }
        $values = implode(",",$values );

        if ($this->db->query("UPDATE `".$this->table."` set ".$values." where id='$id'"))
            return true;
        else
            throw new ModelException($this->db->error);
    }

    /**
     * @param int $id
     * @return array|null
     */
    public function get(int $id): ?array{
        $id = $this->db->real_escape_string($id);
        $res = $this->db->query("SELECT * FROM `".$this->table."` WHERE id='$id'");
        if ($res)
            return $res->fetch_assoc();
        else
            return null;
    }

    /**
     * get record from db by key value
     * @param $key
     * @param $value
     * @return array|null
     */
    public function get_by_key(string $key,string $value):?array {
        $value = $this->db->real_escape_string($value);
        $res = $this->db->query("SELECT * FROM `".$this->table."` WHERE $key='$value'");
        if ($res)
            return $res->fetch_all(MYSQLI_ASSOC);
        else
            return null;
    }

    /**
     * get array of records by given params
     * @param $params
     * @return array|null
     */
    public function get_list(array $params):?array {
        $sql = "SELECT * FROM `".$this->table."`";
        if (isset($params['where'])){
            $sql .= " ".$params['where'];
        }

        if (isset($params['orderby'])){
            $sql .= " ".$params['orderby'];
        }

        if (isset($params['limit'])){
            $sql .= " ".$params['limit'];
        }

        $res = $this->db->query($sql);
        if ($res)
            return $res->fetch_all(MYSQLI_ASSOC);
        else
            return null;
    }

    /**
     * @param int $id
     * @return bool
     * @throws ModelException
     */
    public function delete(int $id):bool {
        $id = $this->db->real_escape_string($id);
        $sql = "DELETE FROM `".$this->table."` WHERE id='$id'";
        if ($this->db->query($sql))
            return true;
        else
            throw new ModelException($this->db->error);
    }
}
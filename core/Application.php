<?php
namespace CodesampleBlog\Core;

use CodesampleBlog\Contracts\ApplicationContract;
use CodesampleBlog\Contracts\ConfigProvider;
use CodesampleBlog\Contracts\EmailServiceContract;
use CodesampleBlog\Contracts\StorageProvider;

/**
 * Class Application
 * @package CodesampleBlog\Core
 */
class Application implements ApplicationContract
{
    /**
     * @var Route route name
     */
    private $route;

    /**
     * @var ConfigProvider config
     */
    private $route_config;

    /**
     * @var StorageProvider database connection
     */
    private $db_connection;

    /**
     * @var EmailServiceContract
     */
    private $email_service;

    /**
     * Application constructor.
     * @param string $route
     * @param ConfigProvider $route_config
     * @param StorageProvider $db_connection
     */
    function __construct(string $route,ConfigProvider $route_config,StorageProvider $db_connection,\Smarty $smarty,EmailServiceContract $email_service)
    {
        $this->route = $route;
        $this->route_config = $route_config;
        $this->db_connection = $db_connection;
        $this->smarty = $smarty;
        $this->email_service  = $email_service;
    }

    /**
     * @return mixed
     */
    function run()
    {
        $controller = $this->route_config->get($this->route);
        $controller_class = new $controller['class']($this->db_connection,$this->smarty,$this->email_service);
        return $controller_class->{$controller['method']}();
    }
}
<?php
namespace CodesampleBlog\Core;

use CodesampleBlog\Contracts\EmailServiceContract;
use CodesampleBlog\Contracts\StorageProvider;
use CodesampleBlog\Exceptions\ViewException;
use CodesampleBlog\Contracts\ControllerContract;

/**
 * Class Controller
 * @package CodesampleBlog\Core
 */
class Controller implements ControllerContract{

    /**
     * @var database connection
     */
    protected $db_connection;

    /**
     * @var \Smarty
     */
    protected $smarty;

    /**
     * @var EmailServiceContract
     */
    protected $email_service;

    /**
     * Controller constructor.
     * @param StorageProvider $db_connection
     */
    function __construct(StorageProvider $db_connection, \Smarty $smarty, EmailServiceContract $email_service)
    {
        $this->db_connection = $db_connection;
        $this->smarty = $smarty;
        $this->email_service = $email_service;
    }

    /**
     * Displays view
     * @param string $view
     * @param string $layout
     * @param array $data
     * @throws ViewException if layout file not found
     */
    function out(string $view,string $layout = 'default',array $data = array()){
        if (!file_exists(__DIR__.'/../views/layouts/'.$layout.'.tpl')){
            throw new ViewException("Layout not found");
        }

        $this->smarty->assign('data',$data);
        $this->smarty->assign('uid',($_SESSION['uid'])??'');
        $this->smarty->assign('body',$this->smarty->fetch($view.".tpl"));

        print $this->smarty->fetch('layouts/'.$layout.'.tpl');
    }

    /**
     * Redirects to given url
     * @param $path url for redirect
     */
    function redirect(string $path){
        header("Location:".$path);
    }
}
<h1>Posts of {$data.author}</h1>
{foreach from=$data.posts item="p"}
    {include file="includes/post.tpl" post=$p type='short'}
{/foreach}
{assign var='url' value="/?action=author-posts&id="|cat:$data['request']['id']}
{include file="includes/navigation.tpl" data=$data url=$url}
<div class="row">
    <div class="col-md-12">
        <h3>
            {$data['post']['post_date']}: {$data['post']['title']}
            {if $data['post']['user_id'] == $uid}
                <a href="/?action=delete-post&id={$data['post']['id']}" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?');">delete</a>
            {/if}
        </h3>
        <p>
            {$data['post']['content']|escape}
        </p>
        <p>Author: {$data['post']['author']}</p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
    <h4>
        {if $data['post']['comments_num']}
            {$data['post']['comments_num']}
        {else}
            No
        {/if}
        comments yet
    </h4>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <ol>
            {foreach from=$data['comments'] item="c"}
                <li>
                    {$c['name']} {if $c['url']}(<a href="{$c.url}" target="_blank">{$c.url}</a>){/if} said: ({$c['post_date']|date_format:"d.m.Y"})
                    {if $uid}{$c.ip}{/if}
                    {if $uid==$c['user_id']}<a href="/?action=delete-comment&id={$c.id}" class="btn btn-xs btn-danger" onclick="return confirm('Are you sure?');">delete</a>{/if}
                    <br>
                    {$c['comment']|escape}
                </li>
            {/foreach}
        </ol>
    </div>
</div>
{if $uid}
<div class="row">
    <div class="col-md-6">
        <h4>Leave a comment</h4>
        <form method="post">
            <div class="form-group">
                <label for="name">Name*</label>
                <input type="text" name="comment[name]" class="form-control" value="{if isset($data['request']['comment']['name'])}{$data['request']['comment']['name']}{else}{$data['userdata'].first_name} {$data['userdata'].last_name}{/if}">
                {if isset($data['errors']['name'])}<small class="error">Field required</small>{/if}
            </div>
            <div class="form-group">
                <label for="comment[email]">Mail</label>
                <input type="text" name="comment[email]" class="form-control" value="{if isset($data['request']['comment']['email'])}{$data['request']['comment']['email']}{else}{$data['userdata'].email}{/if}">
                {if isset($data['errors']['email'])}<small class="error">Field required</small>{/if}
            </div>
            <div class="form-group">
                <label for="comment[url]">URL</label>
                <input type="text" name="comment[url]" class="form-control" value="{if isset($data['request']['comment']['url'])}{$data['request']['comment']['url']}{/if}">
                {if isset($data['errors']['url'])}<small class="error">Field required</small>{/if}
            </div>
            <div class="form-group">
                <label for="comment[comment]">Remark*</label>
                <textarea name="comment[comment]" class="form-control" rows="12">{if isset($data['request']['comment']['comment'])}{$data['request']['comment']['comment']}{else}{/if}</textarea>
                {if isset($data['errors']['comment'])}<small class="error">Field required</small>{/if}
            </div>
            <button type="submit" class="btn btn-default" name="submit" value="1">Submit</button>
        </form>
    </div>
</div>
{/if}
<div class="row">
    <div class="col-md-12">
        <h3>
            {if $type=='short'}
                <a href="/?action=post-details&id={$post['id']}">
            {/if}
            {$post['post_date']}: {$post['title']} {$post['comments_num']}
            {if $type=='short'}
                </a>
            {/if}
        </h3>
        <p>
            {$post['content']|escape|truncate:1000:"..."}
        </p>
        <p>Author: <a href="/?action=author-posts&id={$post['user_id']}">{$post['author']}</a></p>
    </div>
</div>
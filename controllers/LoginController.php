<?php
namespace CodesampleBlog\Controllers;

use CodesampleBlog\Core\Controller;
use CodesampleBlog\Models\User;

/**
 * Class LoginController
 * @package CodesampleBlog\Controllers
 */
class LoginController extends Controller{
    /**
     * login controller
     */
    function index(){
        if (isset($_REQUEST['submit'])){
            $user = new User($this->db_connection->get_connection());
            $user->login($_REQUEST['email'],$_REQUEST['password']);
        }

        if (isset($_SESSION['uid'])){
            $this->redirect("/");
        }

        $this->out('login');
    }
}
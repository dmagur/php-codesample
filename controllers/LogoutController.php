<?php
namespace CodesampleBlog\Controllers;

use CodesampleBlog\Core\Controller;

/**
 * Class LogoutController
 * @package CodesampleBlog\Controllers
 */
class LogoutController extends Controller{
    /**
     * logout controller
     */
    function index(){
        if (isset($_SESSION['uid'])) unset($_SESSION['uid']);
        $this->redirect('/');
    }
}
<?php
namespace CodesampleBlog\Controllers;

use CodesampleBlog\Core\Controller;
use CodesampleBlog\Models\Post;

/**
 * Class HomeController
 * @package CodesampleBlog\Controllers
 */
class HomeController extends Controller{
    /**
     * Home controller
     */
    function index(){
        $post = new Post($this->db_connection->get_connection());

        $pagesize = 3;
        $_REQUEST['page'] = $page = ($_REQUEST['page'])??1;
        $offset = ($page-1)*$pagesize;

        $params = ['orderby' => 'order by post_date desc,id desc','limit' => "LIMIT $pagesize OFFSET $offset"];
        $posts = $post->get_list($params);

        $total_pages = ceil($posts['total_records']/$pagesize);

        foreach ($posts['records'] as $key => $post){
            if ($post['post_date'] == date("Y-m-d"))
                $posts['records'][$key]['post_date'] = "Today";
            else
                $posts['records'][$key]['post_date'] = date("d.m.Y",strtotime($post['post_date']));
        }

        $this->out('home','default',['posts' => $posts['records'],'request' => $_REQUEST,'total_pages' => $total_pages]);
    }

    /**
     * Surface controller
     */
    function surface(){
        $this->out('surface');
    }
}
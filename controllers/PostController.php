<?php
namespace CodesampleBlog\Controllers;

use CodesampleBlog\Core\Controller;
use CodesampleBlog\Models\Comment;
use CodesampleBlog\Models\Post;
use CodesampleBlog\Models\User;

/**
 * Class PostController
 * @package CodesampleBlog\Controllers
 */
class PostController extends Controller{
    /**
     * add post controller
     */
    function add(){
        $errors = array();
        if (isset($_REQUEST['submit']) and $_REQUEST['submit']){
            $post = new Post($this->db_connection->get_connection());
            $_REQUEST['data']['user_id'] = $_SESSION['uid'];
            $_REQUEST['data']['post_date'] = date("Y-m-d H:i:s",strtotime($_REQUEST['data']['post_date']));
            $errors = $post->validate($_REQUEST['data']);
            if (empty($errors)) {
                $id = $post->insert($_REQUEST['data']);
                $this->redirect('/?action=post-details&id='.$id);
                unset($_REQUEST['data']);
            }
        }

        $this->out('add_post','default',['errors' => $errors,'request' => $_REQUEST]);
    }

    /**
     * post details controller
     */
    function details(){
        if (!isset($_REQUEST['id'])) $this->redirect("/");

        $errors = array();
        $comment = new Comment($this->db_connection->get_connection());

        $post = new Post($this->db_connection->get_connection());
        $data = $post->get($_REQUEST['id']);

        if (!$data) $this->redirect("/");

        $author = new User($this->db_connection->get_connection());
        $author_data = $author->get($data['user_id']);
        if (isset($_REQUEST['submit']) and $_REQUEST['submit']){
            $_REQUEST['comment']['user_id'] = $_SESSION['uid'];
            $_REQUEST['comment']['post_id'] = $_REQUEST['id'];
            $_REQUEST['comment']['post_date'] = date("Y-m-d H:i:s");
            $_REQUEST['comment']['ip'] = $_SERVER['REMOTE_ADDR'];
            $errors = $comment->validate($_REQUEST['comment']);
            if (empty($errors)) {
                $comment->insert($_REQUEST['comment']);
                $data['comments_num']++;
                $post->update($data);

                $this->email_service->send($author_data['email'],"Comment by ".$_REQUEST['comment']['name']." to ".$data['title'],$_REQUEST['comment']['comment']);
                unset($_REQUEST['comment']);
            }
        }

        if ($data['post_date'] == date("Y-m-d"))
            $data['post_date'] = "Today";
        else
            $data['post_date'] = date("d.m.Y",strtotime($data['post_date']));

        $data['author'] = $author_data['first_name']." ".$author_data['last_name'];
        $comments = $comment->get_by_key('post_id', $_REQUEST['id']);

        $userdata = [];
        if (!empty($_SESSION['uid'])) {
            $user = new User($this->db_connection->get_connection());
            $userdata = $user->get($_SESSION['uid']);
        }

        $this->out('post_details','default',['post' => $data,'errors' => $errors,'comments' => $comments, 'request' => $_REQUEST,'userdata' => $userdata]);
    }

    function delete(){
        if (!isset($_REQUEST['id'])) {
            $this->redirect("/");
            return;
        }

        $post = new Post($this->db_connection->get_connection());
        $data = $post->get($_REQUEST['id']);

        if (empty($post)){
            $this->redirect("/");
            return;
        }

        if (empty($_SESSION['uid']) or $data['user_id'] != $_SESSION['uid']){
            header("HTTP/1.0 403 Access denied");
            exit;
        }

        $post->delete($_REQUEST['id']);
        $this->redirect("/?action=home");
    }
}
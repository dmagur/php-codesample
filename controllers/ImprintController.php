<?php
namespace CodesampleBlog\Controllers;

use CodesampleBlog\Core\Controller;

/**
 * Class ImprintController
 * @package CodesampleBlog\Controllers
 */
class ImprintController extends Controller{
    /**
     * Imprint controller
     */
    function index(){
        $this->out('imprint');
    }
}
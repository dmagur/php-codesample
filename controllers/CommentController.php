<?php
namespace CodesampleBlog\Controllers;

use CodesampleBlog\Core\Controller;
use CodesampleBlog\Models\Comment;

/**
 * Class CommentController
 * @package CodesampleBlog\Controllers
 */
class CommentController extends Controller{
    /**
     * delete comment
     */
    function delete(){
        if (!isset($_REQUEST['id'])) {
            $this->redirect("/");
            return;
        }

        $comment = new Comment($this->db_connection->get_connection());
        $commentdata = $comment->get($_REQUEST['id']);

        if (empty($commentdata)){
            $this->redirect("/");
            return;
        }

        if (empty($_SESSION['uid']) or $commentdata['user_id'] != $_SESSION['uid']){
            header("HTTP/1.0 403 Access denied");
            exit;
        }

        $comment->delete($_REQUEST['id']);
        $this->redirect("/?action=post-details&id=".$commentdata['post_id']);
    }
}
<?php
namespace CodesampleBlog\Controllers;

use CodesampleBlog\Core\Controller;
use CodesampleBlog\Models\Post;
use CodesampleBlog\Models\User;

/**
 * Class AuthorController
 * @package CodesampleBlog\Controllers
 */
class AuthorController extends Controller{
    /**
     * posts controller
     */
    function posts(){
        if (!isset($_REQUEST['id'])) $this->redirect("/");
        $user = new User($this->db_connection->get_connection());
        $author = $user->get($_REQUEST['id']);
        $author_name = $author['first_name']." ".$author['last_name'];

        $post = new Post($this->db_connection->get_connection());

        $pagesize = 3;
        $_REQUEST['page'] = $page = ($_REQUEST['page'])??1;

        $posts = $post->get_posts_by_author_id($_REQUEST['id'],$_REQUEST['page'],$pagesize);
        $total_pages = ceil($posts['total_records']/$pagesize);

        $this->out('author_posts','default',['posts' => $posts['records'],'total_pages' => $total_pages,'request' => $_REQUEST,'author' => $author_name]);
    }
}
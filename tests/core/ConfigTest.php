<?php
namespace CodesampleBlog\Tests\Core;

use PHPUnit\Framework\TestCase;
use CodesampleBlog\Core\Config;
use CodesampleBlog\Exceptions\ConfigException;

/**
 * Class ConfigTest
 * @package CodesampleBlog\Tests\Core
 */
class ConfigTest extends TestCase{
    /**
     * @test
     */
    public function testNotExistingConfig(){
        $this->expectException(ConfigException::class);
        $config = new Config('notexsiting');
    }

    /**
     * @test
     */
    public function testExistingParam(){
        $config = new Config('database');
        $this->assertNotEmpty($config->get('dbhost'));
    }

    public function notExistingParam(){
        $config = new Config('database');
        $this->assertEmpty($config->get('notexistingparam'));
    }
}
<?php
namespace CodesampleBlog\Tests\Core;

use PHPUnit\Framework\TestCase;
use CodesampleBlog\Core\Config;
use CodesampleBlog\Core\MySqliDatabase;

class MySqliDatabaseTest extends TestCase{
    /**
     * @test
     */
    public function testConnection(){
        $config = new Config('database');
        $storage = new MySqliDatabase($config);
        $this->assertInstanceOf(\mysqli::class,$storage->get_connection());
    }
}
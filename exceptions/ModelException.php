<?php

namespace CodesampleBlog\Exceptions;

/**
 * Class ModelException
 * @package CodesampleBlog\Exceptions
 */
class ModelException extends \Exception{
}
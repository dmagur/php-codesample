<?php

namespace CodesampleBlog\Exceptions;

/**
 * Class ViewException
 * @package CodesampleBlog\Exceptions
 */
class ViewException extends \Exception{
}
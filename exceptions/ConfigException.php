<?php

namespace CodesampleBlog\Exceptions;

/**
 * Class ConfigException
 * @package CodesampleBlog\Exceptions
 */
class ConfigException extends \Exception{
}

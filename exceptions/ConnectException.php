<?php

namespace CodesampleBlog\Exceptions;

/**
 * Class ConnectException
 * @package CodesampleBlog\Exceptions
 */
class ConnectException extends \Exception{
}